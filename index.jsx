let audioEvents = [];
for (const key in Audio.prototype)
  audioEvents.push(key);
audioEvents = audioEvents
  .filter((key) => key.startsWith('on') && !/^on(?:mouse|key|pointer|focus|blur)/.test(key))
  .map((key) => key.slice(2));

let networkStates = Object.fromEntries(Object.keys(HTMLMediaElement.prototype)
  .filter((name) => name.startsWith('NETWORK_'))
  .map((name) => [HTMLMediaElement.prototype[name], name]));

let readyStates = Object.fromEntries(Object.keys(HTMLMediaElement.prototype)
  .filter((name) => name.startsWith('HAVE_'))
  .map((name) => [HTMLMediaElement.prototype[name], name]));

function main() {
  const mount = document.getElementById('app');
  ReactDOM.render(<App />, mount);
}

function App() {
  const [src, setSrc] = React.useState();
  const [audio, setAudio] = React.useState();
  const [entries, setEntries] = React.useState(() => []);

  React.useEffect(() => {
    function listener(event) {
      console.log(event);
      const entry = {
        timestamp: performance.now(),
        event,
        networkState: audio.networkState,
        readyState: audio.readyState,
      };
      setEntries((entries) => [...entries, entry].slice(-1000));
    }

    if (audio)
      for (const event of audioEvents)
        audio.addEventListener(event, listener);

    return () => {
      if (audio)
        for (const event of audioEvents)
          audio.removeEventListener(event, listener);
    };
  }, [audio]);

  return (
    <React.Fragment>
      <Sourcer setSrc={setSrc} />
      <audio ref={setAudio} src={src} controls={true} />
      <EventTable entries={entries} />
    </React.Fragment>
  );
}

function Sourcer({ setSrc }) {
  function handleClick() {
    const clientID = localStorage.getItem('playground-soundcloud-client-id');
    if (!clientID)
      alert('need client id!');
    setSrc('https://api.soundcloud.com/tracks/278216785/stream?client_id=' + encodeURIComponent(clientID));
  }

  return (
    <div>
      <button type="button" onClick={handleClick}>Set src</button>
    </div>
  );
}

function EventTable({ entries }) {
  return (
    <table border={1}>
      <thead>
        <tr>
          <th>Time</th>
          <th>Event</th>
          <th>Network state</th>
          <th>Ready state</th>
        </tr>
      </thead>
      <tbody>
        {entries.map((entry) => <EventRow entry={entry} />)}
      </tbody>
    </table>
  )
}

function EventRow({ entry: { timestamp, event, networkState, readyState } }) {
  return (
    <tr>
      <td>{Math.round(timestamp)}</td>
      <td>{event.type}</td>
      <td>{networkState} – {networkStates[networkState]}</td>
      <td>{readyState} – {readyStates[readyState]}</td>
    </tr>
  );
}

main();
